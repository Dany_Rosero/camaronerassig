<!-- rincondelcodigo.com -->
<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include('conexion.php');
?>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0B1WUc7iL-NvQUvfEPlWi60Zm5qalmj4"></script>
  <link href="styles.css" rel="stylesheet">
</head>
<body>
      
      <form action="index.php" method="get" name="hola">
      <input type="text" placeholder="Latitud" name="latitud"> 
      <input type="text" placeholder="Longitud" name="longitud">
      <input type="submit" value="Buscar Puntos">
      </form> 

    

      <?php 
      $lat =  $row['Lng']; 
      $lng = $row['Lat'];
      $pos = $lat.",".$lng;
      echo "<div class='titular'>Visualizar punto</div>
      <div id='info'>".$pos."</div>
      <div id='googleMap'></div>
     
      ?>
    
      <a href="mapa.php" target="_self"> <input type="button" name="boton" value="Visualizar Puntos Ingresados" /> </a> 
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>
    $(document).ready(function(){
      lat = "<?php echo $lat; ?>" ;
      lng = "<?php echo $lng; ?>" ;
      var map;
      function initialize() {
        var myLatlng = new google.maps.LatLng(lat,lng);
        var mapOptions = {
          zoom: 7,
          center: myLatlng,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);
        var marker = new google.maps.Marker({
          position: myLatlng,
          draggable:true,
          animation: google.maps.Animation.DROP,
          web:"Localización geográfica!",
          icon: "marker.png"
        });
        google.maps.event.addListener(marker, 'dragend', function(event) {
          var myLatLng = event.latLng;
          lat = myLatLng.lat();
          lng = myLatLng.lng();
          document.getElementById('info').innerHTML = [
          lat,
          lng
          ].join(', ');
        });
        marker.setMap(map);
      }
      
    });
</script>
</body>
</html>