<html lang="es">
<head>
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="../css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0B1WUc7iL-NvQUvfEPlWi60Zm5qalmj4"></script>
  <link href="styles.css" rel="stylesheet">
  <script type="text/javascript">
  function load() {
    var map = new google.maps.Map(document.getElementById("map"), {
      center: new google.maps.LatLng(0.93333,-79.68333),
      zoom: 7,
      mapTypeId: 'roadmap'
    });
    var infoWindow = new google.maps.InfoWindow;
    downloadUrl("markers.php", function(data) {
      var xml = data.responseXML;
      var markers = xml.documentElement.getElementsByTagName("marker");
      for (var i = 0; i < markers.length; i++) {
        var point = new google.maps.LatLng(
          parseFloat(markers[i].getAttribute("Lat")),
          parseFloat(markers[i].getAttribute("Lng")));
        var icon = 'marker.png';
        var marker = new google.maps.Marker({
          map: map,
          position: point,
          icon: icon
        });
      }
    });
  }
  function downloadUrl(url, callback) {
    var request = window.ActiveXObject ?
    new ActiveXObject('Microsoft.XMLHTTP') :
    new XMLHttpRequest;
    request.onreadystatechange = function() {
      if (request.readyState == 4) {
        request.onreadystatechange = doNothing;
        callback(request, request.status);
      }
    };
    request.open('GET', url, true);
    request.send(null);
  }
  function doNothing() {}
  </script>
</head>
<center><h1>Camaroneras Ingresadas</h1></center> 
<body onload="load()" style="background-color: lavender">
  <div id="map"></div>
  <div class="button darken-2" align="center" >

      <a class="waves-effect waves-light btn-large light-blue" href="../index.php">Inicio</a>
    
        
      </div>
</body>
</html>