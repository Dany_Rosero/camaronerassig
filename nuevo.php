<html>
	<head>
		<title>reg</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0B1WUc7iL-NvQUvfEPlWi60Zm5qalmj4" charset="UTF-8"></script><script type="text/javascript"
  			src="http://maps.googleapis.com/maps/api/js?sensor=false">
		</script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0B1WUc7iL-NvQUvfEPlWi60Zm5qalmj4"></script>


		<script type="text/javascript">
	  		var mapa;
	  		var opcionesMapa;
			function InicializarMapa() {
				//declaramos un objeto de la clase google.maps.LatLng con la posicion
			    var AVBCPosition = new google.maps.LatLng(0.93333,-79.68333);
			    
			    //define las opciones del mapa, el centro, el zoom y el tipo (ROADMAP)
			    opcionesMapa = {
			        center: AVBCPosition,
			        zoom: 11,
			        mapTypeId: google.maps.MapTypeId.ROADMAP
			    }
			    //define un objeto de la clase google.maps.Map (el mapa) y define en que elemento aparece y cuales son las opciones
			    mapa = new google.maps.Map(document.getElementById("map_canvas"), opcionesMapa);
			    
			    //define un marcador para esa posicion inicial
			    var opcionesMarcador = {
			        position: AVBCPosition,
			        map: mapa,
			        draggable:true,
			        animation: google.maps.Animation.DROP,
			        title: "Hola."
			    }
			    //define un objeto de la clase google.maps.Maker 
			    var marcador = new google.maps.Marker(opcionesMarcador);
			    // activa la escucha del evento clik del raton para que muestre las coord del punto
			    google.maps.event.addListener(mapa, "click", function(evento) {
			        // mostrar las coordenadas del punto seleccionado
			        MostrarCoordenadas(mapa, evento.latLng);
			    });
			    //muestra las coordenadas en la infoventana de google maps
				function MostrarCoordenadas(mapa, latLng) {
				    var opcionesVentana = {
				        content: "Las coordenadas del punto son:<br>(" +
				            latLng.lat() + " &deg;N, " +
				            latLng.lng() + " &deg;E)",

				        position: latLng
				        
				    }
				    document.nuevo_usuario.Lng.value = latLng.lng();

						document.nuevo_usuario.Lat.value = latLng.lat();
				    //define un objeto de la clase google.maps.infoWindow
				    var ventanaInfo = new google.maps.InfoWindow(opcionesVentana);
				    
				    ventanaInfo.open(mapa);
				}
				//========MUESTRA LA POSICION DEL NAVEGADOR===============
				var botonGET=document.getElementById('getPosition');
				botonGET.addEventListener('click', obtenerPosicion, false);
			
				function obtenerPosicion(){
					var geoconfig={
					    enableHighAccuracy: true,
					    maximumAge: 60000
				  	};
				  	control=navigator.geolocation.watchPosition(mostrar, errores, geoconfig);
				}
				function mostrar(posicion){
				  	var ubicacion=document.getElementById('ubicacion');
					var datos='';
					datos+='Latitud: '+posicion.coords.latitude+'<br>';
					datos+='Longitud: '+posicion.coords.longitude+'<br>';
					datos+='Exactitud: '+posicion.coords.accuracy+'mts.<br>';
					document.nuevo_usuario.Lng.value = posicion.coords.longitude;

					document.nuevo_usuario.Lat.value = posicion.coords.latitude;
					ubicacion.innerHTML=datos;
				  	var miPosicion = new google.maps.LatLng(
				  		posicion.coords.latitude,posicion.coords.longitude
			        );
				  	//centra el mapa en esa posicion, tambien funciona con:
			        //mapa.map.setCenter(miPosicion, 11);
			        mapa.panTo(miPosicion);
			        mapa.setZoom(11);
			        //define un marcador para esa posicion 
				    var opcionesMarcadorMe = {
				        position:miPosicion,
				        map: mapa,
				        draggable:true,
				        animation: google.maps.Animation.DROP,
				        title: "HOLA MUNDO, estoy aquí."
				    }
				    //define un objeto de la clase google.maps.Maker 
				    var marcadorMe = new google.maps.Marker(opcionesMarcadorMe);
				}
				function errores(error){
				  alert('Error: '+error.code+' '+error.message);
				}
				//=====EL CENTRO DEL MAPA MUESTRA UNA POSICION AL AZAR======
				var botonRANDOM=document.getElementById('randomPosition');
				botonRANDOM.addEventListener('click', PosicionRandom, false);
				function PosicionRandom () {
					var surOeste = new google.maps.LatLng(51.203405,12.244141);
					var norEste = new google.maps.LatLng(-25.363882,180.044922);
					var longSpan = norEste.lng() - surOeste.lng();
					var latSpan = norEste.lat() - surOeste.lat();
					var randomLat =surOeste.lat() + latSpan * Math.random();
					var randomLon =surOeste.lng() + longSpan * Math.random();
					var randomlocation = new google.maps.LatLng(randomLat,randomLon);
					
					marcador.setPosition(randomlocation);
					//mapa.setCenter(randomlocation, 8);
			        //mapa.panTo(randomlocation);
			        mapa.setCenter(marcador.getPosition());
			        mapa.setZoom(5);
					var datos='';
					datos+='Latitud: '+randomLat+'<br>';
					datos+='Longitud: '+randomLon+'<br>';
					
					document.getElementById('ubicacion').innerHTML=datos;   

					document.nuevo_usuario.Lng.value = randomLon;

					document.nuevo_usuario.Lat.value = randomLat;
				}
				//=====INTRODUCE COORDENADAS DEL LUGAR QUE SE QUIERE MOSTRAR======
				var botonGO=document.getElementById('goToPosition');
				botonGO.addEventListener('click', muestraCoord, false);
				function muestraCoord () {
					document.getElementById('coordenadas').style.display = 'block';
				}
				document.getElementById('go').addEventListener('click', goToCoord, false);
				function goToCoord (){
					var cLat =document.getElementById('Latitud').value;
					var cLon =document.getElementById('Longitud').value;
					var destino = new google.maps.LatLng(cLat,cLon);
					
					marcador.setPosition(destino);
			        mapa.setCenter(marcador.getPosition());
			        mapa.setZoom(5);
					var datos='';
					datos+='Latitud: '+cLat+'<br>';
					datos+='Longitud: '+cLon+'<br>';
					
					document.getElementById('ubicacion').innerHTML=datos;   
				}
			}	
		</script>

<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" /><!-- Bootstrap stylesheet -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" /><!-- stylesheet -->
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" /><!-- fontawesome -->  
<!-- meta tags -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Estate Group Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //meta tags -->
<!--fonts-->

<link href="//fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<!--//fonts-->	
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script><!-- Required-js -->
<script src="js/bootstrap.min.js"></script><!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script type="text/javascript" src="js/numscroller-1.0.js"></script>
	<!-- here stars scrolling icon -->
			<script type="text/javascript">
				$(document).ready(function() {
					/*
						var defaults = {
						containerID: 'toTop', // fading element id
						containerHoverID: 'toTopHover', // fading element hover id
						scrollSpeed: 1200,
						easingType: 'linear' 
						};
					*/
										
					$().UItoTop({ easingType: 'easeOutQuart' });
										
					});
			</script>
			<!-- start-smoth-scrolling -->
			<script type="text/javascript" src="js/move-top.js"></script>
			<script type="text/javascript" src="js/easing.js"></script>
			<script type="text/javascript">
				jQuery(document).ready(function($) {
					$(".scroll").click(function(event){		
						event.preventDefault();
						$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
					});
				});
			</script>
			<!-- start-smoth-scrolling -->
		<!-- //here ends scrolling icon -->


		<!--hasta aqui



		 <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>
        <body style="background-color: lavender" onload="InicializarMapa()">

	


		
		<center><h1>Nuevo registro</h1></center>
			<div style="text-align: center;">
                            <button id="getPosition" class="btn blue">Obtener mi posicion</button>
                            <button id="randomPosition" class="btn blue">Obtener posicion al azar</button>
		    <!--  <button id="goToPosition">Ir a una determinada posicion</button> -->
                    <br> <section id="ubicacion" style="height: 50px; width: 250px" class="section large" >
		  </section>
		  <div id="coordenadas" style="display:none">
		  	Latitud<input id="Latitud" name="Latitud" placeholder="4.3922157">
		  	Longitud<input id="Longitud" name="Longitud" placeholder="-2.2045689">
		    <button id="go">GO</button>

		  </div>
		  <div id="map_canvas" style="width: 500px; height: 200px; background-color:#666; margin: 0 auto;"></div>
		  <div><p>Dar clic para obtener coordenadas...</p></div>
		</div>
		<form name="nuevo_usuario" method="POST" action="guarda_usuario.php" >
                    <table width="100%" style="width: 400px; height: 100px" class="table-of-contents" align="center">
				<tr>
                                    <td><b>Nombre</b></td><td><input type="text" name="Nombre" size="15" /></td><tr>
                                    <td><b>Direccion</b></td><td><input type="text" name="Direccion" size="15" /></td><tr>
                                    <td><b>Lat</b></td><td><input type="text"  name="Lat" size="15" value="" /></td><tr>
                                    <td><b>Lng</b></td><td><input type="text" name="Lng" size="15" value="" /></td><tr>
				<tr>

				</tr>
			

			</table>
                    <td><center> <button class="btn waves-effect green" type="submit" name="action">Guardar </button> <a class="btn waves-effect blue" href="index.php">Regresar</a> </center></td>

		</form>
		<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
	</body>
</html>						
