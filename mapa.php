<?php
	
	require('conexion.php');
	
	$id=$_GET['id'];
	
	$query="SELECT Nombre, Direccion, Lat, Lng, Pos FROM reg WHERE id='$id'";
	
	$resultado=$mysqli->query($query);
	
	$row=$resultado->fetch_assoc();
	
?>

<html lang="es">
<head>
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0B1WUc7iL-NvQUvfEPlWi60Zm5qalmj4"></script>
  <link href="styles.css" rel="stylesheet">
  
</head>

<body style="background-color: lavender">
   <?php 
      $lat = $row['Lat'];
      $lng = $row['Lng']; 
      $pos = $lat.",".$lng;
      echo "<center><h1>Registros de Camaroneras Zona Norte</h1></center>
        <table style='border-color: black'>
        <tr>
            <td>Posicion:</td>
            <td>".$pos."</td>
        </tr>
        <tr>
            <td>Nombre:</td>
            <td>".$row['Nombre']."</td>
        </tr>
        <tr>
            <td>Direccion:</td>
            <td>".$row['Direccion']."</td>
        </tr>
        </tabble>

      <div id='googleMap'></div>

      <div id='respuesta'></div>";
      ?>
 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

    <script>
    $(document).ready(function(){
      lat = "<?php echo $lat; ?>" ;
      lng = "<?php echo $lng; ?>" ;
      var map;
      function initialize() {
        var myLatlng = new google.maps.LatLng(lat,lng);
        var mapOptions = {
          zoom: 15,
          center: myLatlng,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);
        var marker = new google.maps.Marker({
          position: myLatlng,
          draggable:true,
          animation: google.maps.Animation.DROP,
          web:"Localización geográfica!",
          icon: "marker.png"
        });
        google.maps.event.addListener(marker, 'dragend', function(event) {
          var myLatLng = event.latLng;
          lat = myLatLng.lat();
          lng = myLatLng.lng();
          document.getElementById('info').innerHTML = [
          lat,
          lng
          ].join(', ');
        });
        marker.setMap(map);
      }
      google.maps.event.addDomListener(window, 'load', initialize);
      $("#enviar").click(function() { 
        var url = "cargar.php";
        $("#respuesta").html('<img src="cargando.gif" />');
        $.ajax({
         type: "POST",
         url: url,
         data: 'lat=' + lat + '&lng=' + lng,
         success: function(data)
         {
           $("#respuesta").html(data);
         }
       });
      }); 
    });
</script>
<div class="button darken-2" align="center" ><br>

      <a class="btn blue" href="index.php">Inicio</a>
    
        
      </div>
<br>
</body>
</html>