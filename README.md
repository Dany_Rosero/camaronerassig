# CONTROL DE CAMARONERAS

Este sistema esta desarrollado en PHP y MYSQL. esta dise�ado para el registro de camaroneras mediante la utilizacion de la API de google MAPS.
El sistema permite el ingreso de datos basicos, como son:
	- El nombre de la camaronera
	- La direccion de la camaronera
	- Y la latitud y longitud de la ubicacion de la camaronera (esta se obtiene dando clic en el mapa)

Para su instalaci�n se debe:
1) Importar el archivo mapas.sql en un servidor de bases de datos compatible con MySql.
2) Copiar la carpeta completa en el directorio desde donde se ejecute su servidor.
3) Configurar el archivo conexion.php con los parametros correspondientes a su caso.
4) INgresar a la aplicacion por medio del navegador anteponiendo la direccion del servidor que esta utilizando.