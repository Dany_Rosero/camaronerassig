-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-01-2017 a las 23:39:06
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mapas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reg`
--

CREATE TABLE `reg` (
  `id` int(11) NOT NULL,
  `Nombre` varchar(30) COLLATE utf8_bin NOT NULL,
  `Direccion` varchar(30) COLLATE utf8_bin NOT NULL,
  `Lat` varchar(15) COLLATE utf8_bin NOT NULL,
  `Lng` varchar(15) COLLATE utf8_bin NOT NULL,
  `Pos` varchar(30) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `reg`
--

INSERT INTO `reg` (`id`, `Nombre`, `Direccion`, `Lat`, `Lng`, `Pos`) VALUES
(11, 'Lagarto 1', 'Las Mareas - Lagarto', '1.0679794771714', '-79.26721572875', '1.0679794771714008,-79.2672157'),
(12, 'La morenita 1', 'Bocana de Lagarto', '1.0690950820877', '-79.24953460693', '1.0690950820877436,-79.2495346'),
(13, 'Montalvo 01', 'Montalvo - Via principal', '1.0521517877940', '-79.31008815765', '1.0521517877940494,-79.3100881'),
(14, 'Boca del acha', 'Africa - Montalvo', '1.0640498265622', '-79.30240631103', '1.0640498265622227,-79.3024063'),
(15, 'Lagarto 3', 'Las Mareas - Lagarto', '1.0730426039185', '-79.26009178161', '1.0730426039185985,-79.2600917'),
(16, 'Montalvo 02', 'Bocana de Lagarto', '1.0703143800051', '-79.25146579742', '1.0703143800051844,-79.2514657'),
(17, 'Lagarto 2', 'Las Mareas - Lagarto', '1.0699282092718', '-79.26498413085', '1.0699282092718774,-79.2649841'),
(18, 'Lagarto 4', 'Las Mareas - Lagarto', '1.075445440801', '-79.25854682922', '1.075445440801,-79.25854682922'),
(19, 'Africa 1', 'Africa - Montalvo', '1.0752058717456', '-79.28845882415', '1.0752058717456434,-79.2884588'),
(20, 'Africa 2', 'Africa - Montalvo', '1.0739490305736', '-79.28674221038', '1.0739490305736556,-79.2867422');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `reg`
--
ALTER TABLE `reg`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `reg`
--
ALTER TABLE `reg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
